function [ rotatedPositions, IDX ] = stitching_I(IDX, rotatedPositions, stitch_I_opt)
%CLUSTERINGLINES Summary of this function goes here
%   Detailed explanation goes here
for placeholder = 1:5
    u = 1;
    
    while u <= max(IDX)                                                     % Small clusters (defined by var minClusterSize)
        isi = (IDX == u);                                                   % are marked as noise, the IDX vector is changed so that there
        if sum(isi) < stitch_I_opt.minClusterSize                                        % are no numbers 1 <= n <= max(IDX) for which sum(IDX == n) is
            IDX = IDX .* ~isi;                                              % equal to zero.
            IDX(IDX > u) = IDX(IDX > u)-1;
        else
            u = u + 1;
        end
    end
    rotatedPositions = rotatedPositions(IDX>0,:);                           % Remove the noise.
    IDX = IDX(IDX>0);
    
    % This is to stitch clusters together to form whiskers.
    if isempty(IDX)
            continue;
    end
    maxIDX = max(IDX);
    assert(maxIDX<1000)
    scoreMatrix = ones(maxIDX)*Inf;
    tips = cell(1,maxIDX);
    tips = coder.nullcopy(tips);
    for q = 1:maxIDX
        tips{q} = struct('endPoint',[0 0],'beginPoint',[0 0],'R',ones(2),'angle',0,'Length',0);
        tips{q} = rotationData(rotatedPositions(IDX==q,:),stitch_I_opt.lengthOfTipAndBottom,'t');
    end
    parfor q = 1:maxIDX          % WHISKER TIPS
  %       figure(1)
  %       clf
  %       scatter(rotatedPositions(IDX~=q,1),rotatedPositions(IDX~=q,2),'.');
  %       hold on
  %       scatter(rotatedPositions(IDX==q,1),rotatedPositions(IDX==q,2),'.','r');
        scoreMatrix_l = ones(maxIDX,1)*Inf;
        for r = 1:maxIDX      % WHISKER BOTTOMS
            if q == r || isempty(tips{r}) || isempty(tips{q})
                continue;
            end
            bp_rot_r = tips{q}.R*[tips{r}.beginPoint-tips{q}.endPoint]';
            if bp_rot_r(1) < -5
                continue;
            end
            d_x = bp_rot_r(1);
            d_y = bp_rot_r(2);
            r_bottom = rotationData(rotatedPositions(IDX==r,:),stitch_I_opt.lengthOfTipAndBottom,'b');
            if isempty(r_bottom)
                %scoreMatrix(q,r) = Inf;% REMOVE PARFOR
                scoreMatrix_l(r) = Inf; % ONLY PARFOR
                continue;
            end
            beta = abs(r_bottom.angle-tips{q}.angle);
            if beta > stitch_I_opt.maxAD
                continue;
            end
            if abs(d_x) > stitch_I_opt.maxX || abs(d_y) > stitch_I_opt.maxY
                continue;
            end
            d = sqrt(d_x^2+d_y^2);
      %      scoreMatrix(q,r) = d + stitch_I_opt.c*beta; % ONLY PARFOR
             scoreMatrix_l(r) = real(d + stitch_I_opt.c*beta);
    %       scatter(tips{r}.beginPoint(1),tips{r}.beginPoint(2));%,'b');
    %       pause
        end
        %[v,ind] = min(scoreMatrix(q,:)); % REMOVE PARFOR
    %    [v,ind] = min(scoreMatrix_l);
        scoreMatrix(q,:) = scoreMatrix_l;
%         if v < 100000
%             scatter(tips{ind}.beginPoint(1),tips{ind}.beginPoint(2),'r');
%             pause;
%         end
        
        
    end
    minVal = 0;
    theseAreTheSame = zeros(0,2);
    while minVal < 100000
        [minVal,indices] = minmat(scoreMatrix);
        if minVal > 100000
            break;
        end
        theseAreTheSame = [theseAreTheSame; max(indices) min(indices)];
        scoreMatrix(indices(1),:) = ones(size(scoreMatrix(indices(1),:)))*Inf;
        scoreMatrix(:,indices(2)) = ones(size(scoreMatrix(:,indices(2))))*Inf;
    end
    
    theseAreTheSame = sortrows(theseAreTheSame,2,'descend');
    if isempty(theseAreTheSame)
        return;
    end
    for q = 1:size(theseAreTheSame,1)                                   % and work through the list, assigning both members of a pair the same number,
        
        IDX(IDX == max(theseAreTheSame(q,:))) = min(theseAreTheSame(q,:));        % which is equal to the lower of the two.
    end
    u = 1;
    while u <= max(IDX)                                                 % And as before, make sure that for each n > 1 and <= max(IDX), sum(IDX==n) > 0.
        isi = (IDX == u);
        if sum(isi) < stitch_I_opt.minClusterSize
            IDX = IDX .* abs(isi-1);
            IDX(IDX > u) = IDX(IDX > u)-1;
        else
            u = u + 1;
        end
    end
end


