function [order, missedClusters, missedWhiskers] = kalmanFitting(lineGatherer_tracker, ...
    lineGatherer_detector, opts)
%KALMANFITTING This function predicts the position of whisker
%parametrization, then matching them with really detected whiskers based on
%a score, consisting of weighted and normalized differences between the
%predictions and the really detected parameters
% lineGatherer_tracker  : contains parametrizations from the previous frame
% lineGatherer_detector : contains detected whisker parametrizations in the current frame.
% opts                  : contains additional parameters

%% Make predictions
%[track_opt.predictions_Xf, track_opt.predictions_a, track_opt.predictions_B, track_opt.predictions_L] = makePrediction(track_opt,lineGatherer_tracker);
order = zeros(length(lineGatherer_tracker),1);                              % prepare variables: order

occMat = zeros(length(lineGatherer_detector), ...
    length(lineGatherer_tracker));                                          % prepare `occupied' matrix
distMat = zeros(length(lineGatherer_detector), ...
    length(lineGatherer_tracker));                                          % score matrix.

predictions_Xf = zeros(size(lineGatherer_tracker));                         % Prepare vectors for the predictions.
predictions_a = zeros(size(lineGatherer_tracker));
predictions_L = zeros(size(lineGatherer_tracker));
predictions_B = zeros(size(lineGatherer_tracker));

min_Xf = min(cellfun(@(x) x(1),lineGatherer_tracker));                      % Get the minimal and maximal values for all the
max_Xf = max(cellfun(@(x) x(1),lineGatherer_tracker));                      % parameters from the previous frame. This will help in normalization.
min_A = min(cellfun(@(x) x(2),lineGatherer_tracker));
max_A = max(cellfun(@(x) x(2),lineGatherer_tracker));
min_L = min(cellfun(@(x) x(4),lineGatherer_tracker));
max_L = max(cellfun(@(x) x(4),lineGatherer_tracker));
min_B = min(cellfun(@(x) x(3),lineGatherer_tracker));
max_B = max(cellfun(@(x) x(3),lineGatherer_tracker));

normXf = max_Xf-min_Xf;                                                     % Dividing the differences in parameters between the 
normA = max_A-min_A;                                                        % two frames by these values will normalize them,
normL = max_L-min_L;                                                        % which makes it possible to weight them properly
normB = max_B-min_B;

for i = 1:length(lineGatherer_tracker)                                      % for every whisker from frame n-1 we want to track...
    necc_xf = opts.whiskerDB{i}.x1_p;                                       % get the data from earlier frames
    necc_a = opts.whiskerDB{i}.x2_p;
    necc_L = opts.whiskerDB{i}.L;
    necc_b = opts.whiskerDB{i}.x3;
    if length(necc_xf) > opts.kalmanDistance                                % <kalmanDistance> gives the number of frames on which the prediction is based
        necc_xf = necc_xf(1:opts.kalmanDistance);                           % so if there are more frames than that, only select the last <kalmanDistance> ones.
        necc_a = necc_a(1:opts.kalmanDistance);
        necc_L = necc_L(1:opts.kalmanDistance);
        necc_b = necc_b(1:opts.kalmanDistance);
    end
    if length(necc_xf) < opts.kalmanDistance                                % If there are fewer than <kalmanDistance> frames, it doesn't make much sense to use a polynomial fit,
        predictions_Xf(i) = mean(necc_xf);                                  % So we just take a simple mean.
        predictions_a(i) = mean(necc_a);
        predictions_L(i) = mean(necc_L);
        predictions_B(i) = mean(necc_b);
    else
    %    if length(necc_xf) > opts.kalmanDistance                            
    %        proj = arrayfun(@(xf, a, p1, p2) project(xf, a, ...
    %            p1, p2), necc_xf, necc_a,knn.p(1)* ...
    %            ones(size(necc_a)),opts.p(2)*ones(size(necc_a)) ...
   %             ,'UniformOutput',false);                                    % 
   %         necc_a = cellfun(@(x) x(2),proj);
   %        necc_xf = cellfun(@(x) x(1),proj);
   %     end
        p = polyfit(1:length(necc_xf),necc_xf,2);                           % otherwise, we use polyfit for angle and position to predict the situation in the next frame  
        predictions_Xf(i) = p(3);
        p = polyfit(1:length(necc_a),necc_a,2);
        predictions_a(i) = p(3); 
        predictions_L(i) = mean(necc_L);                                    % length and bending do not really change too much, so here, it makes most sense to take an average.
        predictions_B(i) = mean(necc_b);
    end  
end

for i = 1:length(lineGatherer_tracker)                                      % Calculate the scores for each pair of whiskers from n and n-1
    for j = 1:length(lineGatherer_detector)                                 % A lower score mean a higher similarity
            distMat(j,i) = sqrt((opts.weightX*(predictions_Xf(i) ...
                -lineGatherer_detector{j}(1))/normXf)^2 + ...
                               (opts.weightAlpha*(predictions_a(i)- ...
                               lineGatherer_detector{j}(2))/normA)^2 + ...
                               (opts.weightL*(predictions_L(i) - ...
                               lineGatherer_detector{j}(4))/normL)^2 + ...
                               (opts.weightB*(predictions_B(i) - ...
                               lineGatherer_detector{j}(3))/normB)^2);
    end
end

while 1
    [minDist, I] = minmat(distMat);                                         % Get value and indices of field with the lowest value.
    if prod(sum(occMat,1)) || prod(sum(occMat,2)) || ...                    % if all whiskers are matched
            minDist > opts.maxScoreForMatch                                 % or the score becomes too high,
        break;                                                              % then jump out of the loop.
    end
    distMat(I(1),I(2)) = Inf;                                               % Set its value to Inf so that its no longer the lowest value.
    if ~(sum(occMat(I(1),:)) || sum(occMat(:,I(2))))                        % If the whiskers in both frames are still `free', match them
        occMat(I(1),I(2)) = 1;                                              % they're now not free anymore
        order(I(2)) = I(1);                                                 % and they're matche in the `order' vector
    end
end
for i = 1:length(order)                                                     % Check the matches for validity.
    if order(i)                                                             % If the difference between the matched whiskers in terms of position is too big, then unmatch them.
        difference = abs(lineGatherer_detector{order(i)}(1:3) - ...
            lineGatherer_tracker{i}(1:3));
        if difference(1) > opts.maxChangeInXf || ...
                difference(2) > opts.maxChangeInAngle || ...
                difference(3) > opts.maxChangeInBend
            order(i) = 0;
        end
    end
end
missedWhiskers = find(order == 0);                                          % Prepare output
missedClusters = [];
for i = 1:length(lineGatherer_detector)
    if isempty(find(order==i,1))
        missedClusters = [missedClusters i];
    end
end

end