function [w] = rotationData( dots, lengthOfTipOrBottom, flag )
%COV_AVG Give a cluster and a flag, and it will return a rotation matrix,
%the begin point, the end point, and the length as cells.
%   Use of 'flag':
%   b: rotation matrix aligns with cluster bottom
%   t: rotation matrix aligns with cluster top
%   o: rotation matrix aligns with overall average of whisker (i.e. eigenvector if
%   largest eigenvalue;

w = struct('endPoint',[0 0],'beginPoint',[0 0],'R',ones(2),'angle',0,'Length',0);
covMat = cov(dots);                                                         % Determine covariance matrix of cluster
[eigenvectors,eigenvalues] = eig(covMat);                                   % And determine its eigenvalues and eigenvectors
eigenvalues = sum(eigenvalues);
[~,I] = max(abs(eigenvalues));                                              % Find largest eigenvalue
ev = eigenvectors(:,I);                                                     % Take the corresponding eigenvector
if ev(2) < 0                                                                
    ev = -ev;                                                               % Eigenvector should always point away from the snout.
end
R_init = [ev(1) ev(2); -ev(2) ev(1)];                                       % Construct rotation matrix
rotatedDots_n = [R_init*dots'];                                               % Rotate all the cluster
[~,I] = max(rotatedDots_n(1,:));                                              % Find its top
[~,J] = min(rotatedDots_n(1,:));                                              % and bottom
w.endPoint = dots(I,:);                                                       % coordinates
w.beginPoint = dots(J,:);
w.Length = sqrt(sum((w.endPoint-w.beginPoint).^2));                             % Determine the approximate length of the cluster (whisker)
if flag == 't'                                                                % Decide whether to align to the top or bottom.
    rotatedDots = [R_init*[dots(:,1)-w.endPoint(1) dots(:,2)-w.endPoint(2)]']';
    selectedDots = dots(rotatedDots(:,1) > -lengthOfTipOrBottom,:);
elseif flag == 'b'
    rotatedDots = [R_init*[dots(:,1)-w.beginPoint(1) dots(:,2)-w.beginPoint(2)]']';
    selectedDots = dots(rotatedDots(:,1) < lengthOfTipOrBottom,:);
end
if flag == 't' || flag == 'b'                                                                 % Repeat procedure, now just with the 40 highest or lowest points (the 'head' or 'tail' of the cluster);
    if size(selectedDots,1) < 2
   %     w = [];
        w.R = ones(2);
    else
        covMat = cov(selectedDots);
        [eigenvectors,eigenvalues] = eig(covMat);
        eigenvalues = sum(eigenvalues);
        [~,I] = max(eigenvalues);
        ev = eigenvectors(:,I);
        ev = real(ev);
        if ev(2) < 0
            ev = -ev;
        end
        w.R = [ev(1) ev(2); -ev(2) ev(1)];
       % w = [1 1];
    end
else
    w.R = R_init;                     % Or return the general rotation matrix, together with the begin- and endpoint and length.
end
if ~isempty(w)
    w.angle = mod(real(acos((w.R(1,1))))+pi,pi);
else
    w.angle = 0;
end
%w.angle = 0.5*pi-atan(ev(2)/ev(1));
%w = struct('endPoint',w.endPoint,'beginPoint',w.beginPoint,'R',w.R,'angle',w.angle,'Length',w.Length);
end

