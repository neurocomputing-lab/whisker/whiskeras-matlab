function [ output_args, bestFitValue ] = fitWhiskers( dots, oldLine, c, changeLength)
%FITWHISKERS Summary of this function goes here
%   Detailed explanation goes here
if c.maxChangeInXf > 0
    shift = linspace(-c.maxChangeInXf,c.maxChangeInXf,40);
    if ~sum(shift==0)
        shift = [shift 0];
    end
else
    shift = 0;
end
if c.maxChangeInAngle > 0
    angle = linspace(-c.maxChangeInAngle,c.maxChangeInAngle,40)*pi/180;
    if ~sum(angle==0)
        angle = [angle 0];
    end
else
    angle = 0;
end
if c.maxChangeInBend > 0
    bend = [linspace(-c.maxChangeInBend,c.maxChangeInBend,10)];
    if ~sum(bend==0)
        bend = [bend 0];
    end
else
    bend = 0;
end
if ~isempty(c)
    maxAngle = pi-c.minAngleToSnout;
    minAngle = c.minAngleToSnout;
end



allcombs_m = zeros(length(shift)*length(angle)*length(bend),4);
countComb = 1;
for j = shift
    for k = angle
        for m = bend
            allcombs_m(countComb,:) = [j k m 0];
            countComb = countComb+1;
        end
    end
end
if isempty(oldLine)
    output_args = 0;
    bestFitValue = 0;
else
    allcombs = allcombs_m+oldLine(1:4);
    allcombs = allcombs((allcombs(:,2) > minAngle) & (allcombs(:,2) < maxAngle),:);
    allcombs = allcombs(abs(allcombs(:,3)) < c.maxBend,:);
    ac1 = gpuArray(allcombs(:,1)');
    ac2 = gpuArray(allcombs(:,2)');
    ac3 = gpuArray(allcombs(:,3)');
    if size(dots,2) == 2
        dots = dots(:,1) + dots(:,2)*sqrt(-1);
    end
    
    dots = gpuArray(dots);
    sqDistAndDist = arrayfun(@(a, b, c, e) distanceToLine(a, b, c, e), ac1, ac2, ac3, dots);
    sqDist = gather(mean(real(sqDistAndDist),1));
    [bestFitValue, bestFit] = min(sqDist);
    if changeLength
        L = gather(max(imag(sqDistAndDist(:,bestFit))));
    else
        L = oldLine(4);
    end
    output_args = [allcombs(bestFit,1:3) L];
end
end

