function [ parametrizations, MSEs ] = clusters2whiskers2( dots_g, IDX, opts)
%CLUSTERS2WHISKERS This function parametrizes clusters of centerline points
% into whiskers defined by four parameters {x,a,b,L}
%   dots_g: all centerline points
%   IDX: clustering
%   opts: struct with additional options
%   parametrizations: cell array; every cell contains either a
%   parametrization, or is empty (if parametrization is not possible).

parametrizations = cell(1,max(IDX));                                        % Define cell array
modelfun = @(b,x)(abs((b(3)*(cos(b(2))* ...                                 % Define cost function
    (real(x)-b(1))+sin(b(2))*imag(x)).^2- ...
    (-sin(b(2))*(real(x)-b(1))+cos(b(2))*imag(x))).^2));

%modelfun = @(b,x)(abs((b(3)*(cos(b(2))*(real(x)-b(1))+sin(b(2))*(imag(x)+D)).^2-(-sin(b(2))*(real(x)-b(1))+cos(b(2))*(imag(x)+D)))));

MSEs = cell(1,max(IDX));
for i = 1:max(IDX)                                                          % for every cluster
    dots = dots_g((IDX==i),:);                                              % Get the centerline points for every cluster
    D = min(dots(:,2));
    dots = dots - [0 D];
    if sum(IDX==i) < opts.minNrOfDots                                       % If there are too few points, parametrization will be unreliable.
        continue;                                                           % Therefore, discard the cluster
    end
 %   if i == 5
 %       i
 %   end
    reb = rotationData(dots, 10, 'o');                                      % Get an estimation of the angle of the whisker, its begin- and endpoints, and rotation matrix R
    clusterL = (reb.endPoint(1)-reb.beginPoint(1))^2+ ...
        (reb.endPoint(2)-reb.beginPoint(2))^2;                              % Get an estimation of the square of the lenght of the whisker, by calculating the distance between tip and bottom.
    if clusterL < (opts.minLength)^2                                        % If the whisker is too short, we are not interested in it (or it might be a hair), therefore, discard the cluster
        continue;
    end
    middle = mean([reb.beginPoint; reb.endPoint]);                          % Estimate position of the middle of the whisker
    xf = middle(1)-middle(2)/((reb.R(1,2))/(reb.R(1,1)));                   % And use that, and the estimated rotation matrix R to estimate the position.                            
    beta0 = [xf reb.angle 0];                                               % Obtain a first estimation of the parametrization
    xd = dots(:,1)+dots(:,2)*1i;                                            % Prepare data for nlinfit
    yd = zeros(size(xd));
    opts_nlinfit = statset('nlinfit');
    opts_nlinfit.Robust = 'on';
    opts_nlinfit.RobustWgtFun = 'talwar';                                  % This is a robust nlfit-algorithm, but other algorithms are available
%         figure(1)
%     clf
%     scatter(dots(:,1),dots(:,2),'.');
%     figure(2)
%     clf
%     myLine = plotParLine([beta0 sqrt(clusterL)]);
%     scatter(myLine(:,1),myLine(:,2),'.');
%     pause;
    [whisker,~,~,~,MSE,~] = nlinfit(xd,yd,modelfun,beta0,opts_nlinfit);                   % Perform non-linear fitting, based on the cost function (earlier defined)
    if abs(whisker(1)) > opts.absMaxX || ...                                % If the parametrization does not comply with the boundary conditions, discard this cluster
            whisker(2) < opts.minAngleToSnout || ...
            whisker(2) > (pi-opts.minAngleToSnout) || ...
            abs(whisker(3)) > opts.maxBend || ...
            MSE > opts.maxMSE
        continue;
    end
    dots_r = [dots - [whisker(1) 0]]*[cos(whisker(2)) ...
        -sin(whisker(2)); sin(whisker(2)) cos(whisker(2))];                 % Determine the tip of the whisker
    x_max = max(abs(dots_r(:,1)));      
    L = integral(@(x)(sqrt(1+(2*whisker(3)*x).^2)),0,x_max);                % Calculate the length of the whisker.
    whisker = [whisker L D];                                            % Add whisker length to the whisker description, completing the tuple. We're also retaining the x_max, which comes in handy for Fit-whisker-point-to-data tracking
    parametrizations{i} = depthTransform(whisker,0);                                          % Save for output
  %  i
   MSEs{i} = MSE;
end

end



