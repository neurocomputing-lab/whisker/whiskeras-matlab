function [ rotatedPositions, IDX ] = clusteringLines( IDX, rotatedPositions, minClusterSize, minClusterLength, cone, alwaysFit, weightOfAngleDiff, fitPar)
%CLUSTERINGLINES Summary of this function goes here
%   Detailed explanation goes here
for placeholder = 1:3
u = 1;
Reb = [];

while u <= max(IDX)                                                     % Small clusters (defined by var minClusterSize)
    isi = (IDX == u);                                                   % are marked as noise, the IDX vector is changed so that there
    if sum(isi) < minClusterSize                                        % are no numbers 1 <= n <= max(IDX) for which sum(IDX == n) is
        IDX = IDX .* ~isi;                                              % equal to zero.
        IDX(IDX > u) = IDX(IDX > u)-1;
    else
        Reb_t = R_end_begin(rotatedPositions(isi,:),0);
        if Reb_t{4} < minClusterLength
            IDX = IDX .* ~isi;
            IDX(IDX > u) = IDX(IDX > u)-1;
        else
            Reb{u} = Reb_t;
            u = u + 1;
        end
    end
end
rotatedPositions = rotatedPositions(IDX>0,:);                           % Remove the noise.
IDX = IDX(IDX>0);
% This is to stitch clusters together to form whiskers.
    if isempty(IDX)
        if start
            disp("ERROR: no whiskers were detected! Terminating.")
        else
            continue;
        end
    end
    endBegin = cell(max(IDX),1);                                        % for every whisker q, there is cell with all the begin point of all whiskers, rotated so that they align with the tail of whisker q.
    endBegin_matrix = zeros(max(IDX));                                  % matrix, to be filled with distances from endpoint to beginpoint.

    parfor q = 1:max(IDX)
        endBegin{q} = Reb{q}{1}*[cell2mat(cellfun(@(x) ...
            x(3),Reb'))-Reb{q}{2}]';
        for j = 1:length(endBegin{q})
            if endBegin{q}(1,j) < -1 || endBegin{q}(1,j) > fitPar.maxX                                  % If a cluster's head lies below another cluster's tail, they can never be part of the same whisker
                endBegin_matrix(q,j) = Inf;                             % Inf, so these clusters won't be grouped together
            elseif max(atan(cone)*endBegin{q}(1,j),alwaysFit)...        % when they are not aligned well, they will not be stitched together
                    < abs(endBegin{q}(2,j))                             % Alignment becomes more flexible if the distance between the clusters is greater.
                endBegin_matrix(q,j) = 10000;
            elseif abs(acos(Reb{q}{1}(1,1))-acos(Reb{j}{1}(1,1))) > fitPar.maxAD
                endBegin_matrix(q,j) = Inf;
            else
                R_J = R_end_begin(rotatedPositions(IDX==j,:),1);
                endBegin_matrix(q,j) = sqrt(endBegin{q}(1,j)^2+endBegin{q}(2,j)^2)+ ...
                    weightOfAngleDiff * abs(atan(Reb{q}{1}(1,1))/atan(Reb{q}{1}(1,2)) - atan(R_J{1}(1,1))/atan(R_J{1}(1,2)));
            end
        end
    end
    reallyTheSmallest = endBegin_matrix == min(endBegin_matrix);        % If two clusters are to be grouped together, the heads and tails should be mutual closest neighbours
    endBegin_matrix = endBegin_matrix + ~reallyTheSmallest*10000;       % If this is not the case, they cannot be grouped together
    [distances, indices] = min(endBegin_matrix,[],2);                   % Find the closest neighbours
    theseAreTheSame = [[1:length(distances)]' indices distances];       % Pair the clusters that are part of the same whisker
    theseAreTheSame = sortrows(sort(theseAreTheSame ...                 % Order the matrix with the highest number on the top left.
        (theseAreTheSame(:,3) < 1000 & (theseAreTheSame(:,1) ~= theseAreTheSame(:,2)),1:2),2,'descend'),1,'descend');
    if isempty(theseAreTheSame)
        break;
    end
    for q = 1:size(theseAreTheSame,1)                                   % and work through the list, assigning both members of a pair the same number,
        IDX(IDX == theseAreTheSame(q,1)) = theseAreTheSame(q,2);        % which is equal to the lower of the two.
    end
    u = 1;
    while u <= max(IDX)                                                 % And as before, make sure that for each n > 1 and <= max(IDX), sum(IDX==n) > 0.
        isi = (IDX == u);
        if sum(isi) < minClusterSize
            IDX = IDX .* abs(isi-1);
            IDX(IDX > u) = IDX(IDX > u)-1;
        else
            u = u + 1;
        end
    end
end
end

