function newWhisker = depthTransform(whisker,D)
%DEPTHTRANSFORM Summary of this function goes here
%   Detailed explanation goes here
    shiftDown = whisker(5)-D;
    newWhisker = [whisker(1:5) shiftDown];
    newWhisker(1) = newWhisker(1)-(shiftDown/(tan(whisker(2))));
end

