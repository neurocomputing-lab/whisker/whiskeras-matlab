function [ output_args ] = create_2DGaussian( sigma, T )
%CREATE_2DGAUSSIAN Creates an T*T matrix with a 2D gaussian, centered in
%the matrix

output_args = zeros(T);
for i = 1:T
    for j = 1:T
        x = i-T/2;
        y = j-T/2;
        output_args(i,j) = exp(-((x^2+y^2)/(2*sigma^2)))/(2*pi*sigma^2);    % Gaussian formula
    end
end

end

