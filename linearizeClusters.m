function [ output_args ] = linearizeClusters( dots, IDX )
%LINEARIZECLUSTERS Summary of this function goes here
%   Detailed explanation goes here
output_args = cell(1,max(IDX));
for i = 1:max(IDX)
    cluster = dots(IDX==i,:);
    covMat = cov(cluster);                                                         % Determine covariance matrix of cluster
    [eigenvectors,eigenvalues] = eig(covMat);                                   % And determine its eigenvalues and eigenvectors
    eigenvalues = sum(eigenvalues);
    [~,I] = max(abs(eigenvalues));                                              % Find largest eigenvalue
    ev = eigenvectors(:,I);                                                     % Take the corresponding eigenvector
    if ev(2) < 0
        ev = -ev;                                                               % Eigenvector should always point away from the snout.
    end
    R_init = [ev(1) ev(2); -ev(2) ev(1)];                                       % Construct rotation matrix
    rotatedDots = [R_init*cluster'];                                               % Rotate all the cluster
    [~,I] = max(rotatedDots(1,:));                                              % Find its top
    [~,J] = min(rotatedDots(1,:));                                              % and bottom
    output_args{i} = [cluster(J,1) cluster(J,2) cluster(I,1) cluster(I,2)];
end

