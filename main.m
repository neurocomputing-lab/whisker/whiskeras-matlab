%% WHISKER TRACKING
%  Author: Jan-Harm Betting
%
%% SETTINGS:
loadNewJob = 1;
if ~loadNewJob
    clearvars -except myJob loadNewJob;
else
    clearvars -except loadNewJob;
    close all
end
clc
warning('off','all')
opts = statset('Display','final');                                          % Prevents errors in GPU operations
%addpath('BWTT_files');                                                      % path with additional files / functions
%addpath('external_methods');
%addpath('submethods');
loadPrevious = 0;                                                           % Load previous results (e.g. if you want to continue)
noPlotting = 0;                                                             % 1 mean no plotting, 0 means every iteration will produce some plots (which is slower)
delayedNoPlotting = 50;                                                      % noPlotting will be made 1 after <delayedNoPlotting> iterations
plotEvery = 200;
pauseOn = -1;% 313;
maxRetrainTime = 30;
if loadPrevious
    clear all
    load('matlab.mat');                                               % load previous file
    loadPrevious = 1;                                                       % set necessary settings, to be able to continue a previous analysis.
    startFrame = i;
    noPlotting = 0;
    delayedNoPlotting = 0;
    train_opt.bootstrap = -1;
    outputFolder = 'Outputs';
    endFrame = myJob.getVideoInfo.NumFrames;
    saveEvery = 50000;
else
    %% PARAMETERS: JOB
    videoPath = '../190910-100444-001-00000-00000.mkv_ROTATED_Original.avi';
    outputFolder = '../Outputs';                                              % path to folder for saving outputs
    startFrame = 200000;%425;                                                      % startframe for video.
    endFrame = 300000;                                                        % endframe. If this is set to 0, it means the video will be analyzed until the last frame
    saveEvery = 50000;                                                        % Save the results if mod(framenumber,<saveEvery>) == 0
    dontTrack = 0;
    %% PARAMETERS: POSITION OF SNOUT
    tip = [227 475];                                                       % Position of the tip of the snout (on the original image)
    middle = [373 683];                                                      % Position of the middle of the snout (on the original image)
    %% PARAMETERS: IMAGE PREPROCESSING:
    % Gaussian blur, deinterlacing
    do_deinterlacing = 0;                                                   % 1 means deinterlacing, 0 means no deinterlacing
    initialGaussianSigma = 0.5;                                             % Gaussian blur. 0 means no blur.
    
    %Masks
    imdilate_value = 5;                                                     % How much of the fur should be shaved?
    cutoff = 4;%150;                                                            % Distance from the snout that whiskers are starting to be detected. Prevents detection of fur as whiskers.
    framing = 10;                                                            % How many pixels of the edges should be removed?
    blindSpots = {};
    % The following 5 parameters are directly adopted from BWTT
    bwThreshold = 0.05;
    bwMorphology = 0.2;
    histEqMinT = 2;
    histEqMaxT = 200;
    histEqGamma = 0.8;
    
    remove_lowlevels = 0;                                                   % Pixels with gray values lower than this will be set to 0;
    
    
    %% PARAMETERS: WHISKER POINT DETECTION
    
    upscaling = 2;                                                        % input variable for interp2. Higher value means more upscaling.
    tr2 = 0.6;                                                              % Limit for Steger centerline point detection. Lower value of <tr2> means more sensitive.
    sigma = 2.4;                                                            % Sigma of the Gaussian kernel. Wider whiskers need a higher value for <sigma)
    gaussianKernelSize = 50;                                                % Size of the Gaussian kernel. Higher values for <sigma> need higher values for this.
    
    %% PARAMETERS: CLUSTERING
    
    % Local clustering (Steger)
    steger_c=1;                                                           % c value for clustering. Distance between points is calculated as |d + c*beta|, in which d is the Cartesian distance and beta is the difference in angle in radians.
    minClusterLength = 15;                                                  % Clusters of which the number of points is lower than this value  will be discarded as noise;
    
    % whisker stitching (1) (stitching clusters together)
    stitch_I_opt.alwaysFit = 2;                                            % For whisker stitching: if the perpendicular offset between two clusters is less than this, always fit them together.
    stitch_I_opt.cone = 2;                                                  % [degr]. As there is more distance between two clusters, the chance of an offset is greater (due to bending). Therefore, the maximum offset is modelled as a cone that becomes wider with greater distance.
    stitch_I_opt.c = 0;                                                    % Similar to <steger_c>, there's a weight for angle [rad] difference,
    stitch_I_opt.maxX = 100;                                                % max distance between tip and bottom, parallel to tip.
    stitch_I_opt.maxY = 5;
    stitch_I_opt.maxAD = pi/10;                                             % max angle difference between tip and bottom.
    stitch_I_opt.lengthOfTipAndBottom = 15;                                 % Length of tip and bottom in px.
    stitch_I_opt.minClusterSize = 5;                                       % Minimum cluster length (in pixels) after local clustering. Clusters shorter than this value will be regarded as noise.
    
    % whisker stitching (2) (detect overlapping centerlines)
    stitch_II_opt.highestRoot = 20;                                         % If a whisker's lowest point is higher than <highestRoot>, try to extend it towards the snout.
    stitch_II_opt.acceptableYdistance = 10;                                  % The procedure works by 'inoculating' a whisker onto the whisker it has been hiding behind. These two
    stitch_II_opt.acceptableXdistance = 50;                                % variables indicate what an acceptable distance between the lowest point of the whisker, and the whisker it was hiding behind is.
    stitch_II_opt.lengthOfTipAndBottom = 20;
    stitch_II_opt.minClusterSize = 20;
    %% PARAMETERS: PARAMETRIZATION
    
    param_opt.minNrOfDots = 40;                                             % Clusters with fewer than <param_opt.minNrOfDots> datapoints will be ignored.
    param_opt.minLength = 125;                                               % Clusters with the distance between tip and bottom smaller than <param_opt.minLength> will be ignored.
    param_opt.minLength2 = 100;
    param_opt.minAngleToSnout = 15;                                          % Parametrizations that do not comply with these requirements will be ignored.
    param_opt.maxBend = 1e-2;
    param_opt.maxMSE = 120;  
    
    %% PARAMETERS: TRACKING AND RECOGNITION
    
    % Recognition: SVM parameters
    train_opt.retrainFreqInit = 1;                                          % Minimum retraining frequency in the beginning (frames/retraining).
    train_opt.retrainAtLeast = 5;                                          % Absolute minimum retraining frequency.
    train_opt.increaseEvery = 100;                                          % Increase <train_opt.retrainFreqInit> with <train_opt.increaseWith> every <train_opt.increaseEvery> frames.
    train_opt.increaseWith = 1;
    train_opt.bootstrap = 100;                                              % In the first <train_opt.bootstrap> only tracking happens; no recognition.
    train_opt.window = 7000;                                                % Sliding window size for training data
    train_opt.maxSizeTrainingData = 3500;                                   % Maximum size of training data per whisker during training. If <train_opt.maxSizeTrainingData> is smaller than <train_opt.window>, the training examples of which the average angle of all the whiskers is most similar to the current situation will be chosen.
    train_opt.chanceTrain = 0.6;
    % Tracking: Fit-whisker-point-to-data-tracking option
    fwptd_opt.maxMatchDistance = 40;                                        % Max average absolute distance ( as a function of real distance and difference in length) between cluster and parametrization.
    fwptd_opt.maxLowestDistance = 10;                                       % Maximum for minimum distance between cluster point and parametrization. If the distance between the closest point and the parametrization is higher than this, matching is impossible.
    twptd_opt.weightPastLength = 0.2;                                       % ??
    twptd_opt.maxPrevDist = 200;                                            % ??
    
    % Tracking: Kalman filter option.
    useKalman = 1;                                                          % If <useKalman> == true, use a Kalman filter for tracking. Otherwise, use Fit-whisker-point-to-data tracking.
    track_opt.kalmanDistance = 10;                                          % Prediction will be based on previous <track_opt.kalmanDistance> frames.
    track_opt.weightB = 0.05;                                               % Weighing of different parameters: bending, length, angle (alpha) and position (X)
    track_opt.weightL = 0.01;
    track_opt.weightAlpha = 1.5;
    track_opt.weightX = 2.5;
    track_opt.maxChangeInAngle = 15;                                         % If the angle between a whisker recognized in frame n differs more than <N_expert_op.maxChangeInAngle> degrees from the one in n-1, the recognition is rejected.
    track_opt.maxChangeInXf = 50;                                           % If the position between a whisker recognized in frame n differs more than <N_expert_op.maxChangeInXf> pixels from the one in n-1, the recognition is rejected.
    track_opt.maxChangeInBend = 0.01;                                      % If the bending parameter b between a whisker recognized in frame n differs more than <N_expert_op.maxChangeInBend> from the one in n-1, the recognition is rejected.
    track_opt.maxScoreForMatch = 50;
    
    % Recognition: weighing
    recog_op.weightX = 1;
    recog_op.weightA = 1;
    recog_op.weightB = 0.05;
    recog_op.weightL = 0.09;
    
    % N-expert
    N_expert_op.maxChangeInAngle = 15;                                       % If the angle between a whisker recognized in frame n differs more than <N_expert_op.maxChangeInAngle> degrees from the one in n-1, the recognition is rejected.
    N_expert_op.maxChangeInXf = 100;                                         % If the position between a whisker recognized in frame n differs more than <N_expert_op.maxChangeInXf> pixels from the one in n-1, the recognition is rejected.
    N_expert_op.maxChangeInBend = 0.01;                                 % If the bending parameter b between a whisker recognized in frame n differs more than <N_expert_op.maxChangeInBend> from the one in n-1, the recognition is rejected.
    
   
    hyperparOp.Optimizer = 'gridsearch';
    hyperparOp.AcquisitionFunctionName = 'probability-of-improvement';
    hyperparOp.MaxObjectiveEvaluations = 3000;
    hyperparOp.MaxTime = 5;
    hyperparOp.NumGridDivisions = 2;
    hyperparOp.ShowPlots = false;
    hyperparOp.SaveIntermediateResults = false;
    hyperparOp.Verbose = 1;
    hyperparOp.UseParallel = false;
    hyperparOp.Repartition = true;
    
    %% CONSTANTS (will be set up before looping through the video)
    
    tDgauss = gaussian2D(sigma,gaussianKernelSize);                         % 2D gaussian kernel
    tr2 = gpuArray(-tr2);                                                   % tr2 goes to GPU. Also, is inverted, because we detect light whiskers on a dark background.
    gx = gpuArray(diff(tDgauss,1,2));                                       % Differentiate the Gaussian kernel in x and y directions,
    gxx = gpuArray(diff(tDgauss,2,2));                                      % and put them in to gpuArrays, so that the convolution
    gxy = gpuArray(diff(gx,1,1));                                           % can happen according to the Steger method
    gy = gpuArray(diff(tDgauss,1,1));
    gyy = gpuArray(diff(tDgauss,2,1));
    
    
    track_opt.whiskerDB = [];                                               % Create empty matrix, to be filled with data points for training
    track_opt.meanNormalization = zeros(1,3);                               % ??
    track_opt.stdNormalization = ones(1,3);                                 % ??
    track_opt.rotationMatrix = eye(2);                                      % ??
    param_opt.minAngleToSnout = param_opt.minAngleToSnout*pi/180;           % Convert parameters from degrees to radians
    track_opt.maxChangeInAngle = track_opt.maxChangeInAngle*pi/180;
    N_expert_op.maxChangeInAngle = N_expert_op.maxChangeInAngle*pi/180;
    fwptd_opt.maxMatchDistance = fwptd_opt.maxMatchDistance.^2;             % Redefine <fwptd_opt.maxMatchDistance> as the square of the distance (for practical purposes)
    fwptd_opt.maxLowestDistance = fwptd_opt.maxLowestDistance.^2;           % idem for <fwptd_opt.maxLowestDistance>
    if delayedNoPlotting                                                    % Redefine <delayedNoPlotting> in terms of the startframe.
        delayedNoPlotting = delayedNoPlotting+startFrame;
    end
    svm_exists = 0;
    start = true;                                                           % initialize, so that some actions are taken the first time
    allResults = [];                                                        % Empty matrix in which the results of the analysis will be put.
    if loadNewJob
        myJob = stJob(videoPath, ...                                            % Load the video using an adapted version of the method from BWTT.
            do_deinterlacing);
    else
        disp(['Using file: ' videoPath]);
    end
    if ~endFrame                                                            % If the end frame for analysis is not defined, define it as the last frame of the video
        endFrame = myJob.getVideoInfo.NumFrames;
    end
end

%% HERE THE ACTUAL PROCESSING STARTS!
for i = startFrame:endFrame                                                 % Loop through all the whiskers
    if i > delayedNoPlotting && delayedNoPlotting > 0
        noPlotting = 1;                                                     % Check if we need to plot after the analysis of this particular frame.
    end
    
    %% STEP 1: LOAD THE FRAME, DO PREPROCESSING
    tic
    disp(['~~~~~~~~~~~~']);
    disp(['Frame ' num2str(i) ' of ' ...
        num2str(myJob.getVideoInfo.NumFrames)]);
    tic
    currentFrame = myJob.getVideoFrames(i);                                 % Get current frame.
    result.time.load = toc;
    tic
    currentFrame = currentFrame{1};                                         % necessary since getVideoFrames returns a cell
    if start                                                                % Processes to be done only at the first frame
        bgImage = stGetBackgroundImage(myJob, do_deinterlacing,startFrame,endFrame);            % Extract background image (BWTT)
        BWmask = ones(size(currentFrame));                                  % create a mask to remove the mouse silhouette
        [~, bwShape] = ...
            stExtractWhiskersGrayValuesWithMorphologyAndShave( ...
            currentFrame,bwThreshold, bwMorphology, ...
            imdilate_value, BWmask);
    end
    whiskersBackgroundRemoved = (bgImage - currentFrame);                   % Remove background from current frame
    whiskersBackgroundAndAnimalRemoved = immultiply( ...                    % Remove the animal
        whiskersBackgroundRemoved, uint8(bwShape));
    if initialGaussianSigma > 0
        whiskersBackgroundAndAnimalRemoved = imgaussfilt( ...
            whiskersBackgroundAndAnimalRemoved,initialGaussianSigma);
    end
    preprocessedImage = imadjust( ...                                       % Do preprocessing according to parameters
        whiskersBackgroundAndAnimalRemoved, ...
        [histEqMinT/255 histEqMaxT/255], ...
        [0 1], ...
        histEqGamma ...
        );
    edgeRemovedImage = remove_edge((preprocessedImage > ...                 % Remove edge of the frame (to remove timestamp, which is regarded as noise for our purpose)
        remove_lowlevels) .* double(preprocessedImage),framing);
    % edgeRemovedImage = edgeRemovedImage(cropTo(1):cropTo(2),cropTo(3):cropTo(4));
    upscaledImage = imresize(edgeRemovedImage, upscaling,'lanczos3');          % Very thin whiskers are difficult to detect by the Steger algorithm. Therefore, upscaling is necessary
    if start                                                                % Redefine some parameters after upscaling (only has to be done once)
        sizeUpscaledImage = size(upscaledImage);
        sizeEdgeRemovedImage = size(edgeRemovedImage);
        imageUpscaling = (sizeUpscaledImage(1)/sizeEdgeRemovedImage(1));
        %     tip = tip*imageUpscaling;
        %     middle = middle*imageUpscaling;                                     % new position of tip-middle line in upscaled image
        origin = [(tip(1)-middle(1))/2+middle(1) ...
            (tip(2)-middle(2))/2+middle(2)];                                % defining new grid for whiskers
        a_value = (tip(2)-middle(2))/(tip(1)-middle(1));
        alpha = atan(a_value);
        b_value = tip(2)-a_value*tip(1);
        sbase = [cos(alpha) sin(alpha); -sin(alpha) cos(alpha)];
        track_opt.maxWhiskerLength = [origin(1)-framing*imageUpscaling];    % Indicates the max length of a whisker. This needs to be done, because whiskers get cut off if they are outside the scope of the camera. It prevents the algorithm from seeing the whiskers as 'growing' as the angle changes.
        param_opt.maxWhiskerLength = track_opt.maxWhiskerLength;
        param_opt.absMaxX = sqrt((tip(1)-middle(1))^2 ...
            +(tip(2)-middle(2))^2)/1.4;
    end
    result.time.preprocessing = toc;
    %% STEP 2: STEGER WHISKER DETECTION
    disp(['Steger structure detection']);
    tic;
    gpuUpscaledImage = gpuArray(upscaledImage);                             % Therefore, the image is upscaled.
    rx = conv2(gpuUpscaledImage,gx,'same');                                 % Convolve the image with Gaussian kernels
    ry = conv2(gpuUpscaledImage,gy,'same');
    rxx = conv2(gpuUpscaledImage,gxx,'same');
    rxy = conv2(gpuUpscaledImage,gxy,'same');
    ryy = conv2(gpuUpscaledImage,gyy,'same');
    y_index = gpuArray((1:sizeUpscaledImage(1))' ...
        .*ones(sizeUpscaledImage));                                         % Also load indices to GPU
    x_index = gpuArray((1:sizeUpscaledImage(2)) ...
        .*ones(sizeUpscaledImage));
    whiskerCells = arrayfun(@(rx, ry, rxx, rxy, ryy, y, x, tr2) ...         % And perform the per-pixel part of the Steger algorithm
        CstegerWhiskerDetect(rx, ry, rxx, rxy, ryy, y, x,tr2), ...
        rx, ry, rxx, rxy, ryy, y_index, x_index, tr2);
    result.time.whisker_point_detection = toc;
    
    %% STEP 3: CLUSTERING
    tic;
    % Local clustering.
    mask = (x_index*a_value+b_value < y_index);
    whiskerCells = whiskerCells.*mask+(mask-1);
    for bl = [1:length(blindSpots)]
        spot = blindSpots{bl};
        x_min = min(spot(1),spot(3));
        y_min = min(spot(2),spot(4));
        x_max = max(spot(1),spot(3));
        y_max = max(spot(2),spot(4));
        whiskerCells(y_min:y_max,x_min:x_max) = 0;
        upscaledImage(y_min:y_max,x_min:x_max) = 0;
    end
    IDX=stegerClustering(whiskerCells, steger_c)';                          % Perform local clustering
 %   result.nrPoints.IDX_1 = length(IDX);
 %   result.nrPoints.IDX_2 = length(unique(IDX));
    whiskerCells = whiskerCells(:);                                         % x and y coordinates were returned as x + yi
    whiskerCells = whiskerCells(real(whiskerCells) > 0);                    % if -1 + 0i is returned, it means there is no whisker detected.
    whiskerCells = gather(whiskerCells);                                    % get the results back from the GPU
    positionsOfWhiskers = [real(whiskerCells) imag(whiskerCells)];          % and put them in an array [x y]
    %  IDX = DBSCAN(positionsOfWhiskers,3,2);
    rotatedPositions = [(sbase)*[positionsOfWhiskers-origin]']' - ...
        [0 cutoff];                                                         % shift and rotate whiskers, so that the line along the snout + offset is defined as the x-axis. Everything under this x-axis will be discarded.
    if isempty(IDX)                                                         % if no clusters were detected ...
        if start                                                            % in the first frame, it doesn't make sense to continue. Terminate.
            disp("ERROR: no whiskers were detected! Terminating.")
            break;
        else
            continue;                                                       % Else, just skip this frame.
        end
    end
    result.time.clustering1 = toc;
    tic
    IDX=IDX(rotatedPositions(:,2) > 0);                                     % Discard all whisker pixels under the x axis.
    rotatedPositions = rotatedPositions(rotatedPositions(:,2) > 0,:);
    IDX_lc = IDX;
    rotatedPositions_lc = rotatedPositions;
    [ rotatedPositions, IDX ] = stitching_I( IDX, rotatedPositions, ...     % Whisker stitching I
        stitch_I_opt);
    [ rotatedPositions, IDX ] = stitching_II( rotatedPositions, IDX, ...    % Whisker stitching II
        stitch_II_opt);
    IDX_c2 = IDX;
    %  result.nrPoints.IDX_3 = length(unique(IDX_c2));
    rotatedPositions_c2 = rotatedPositions;
    result.time.clustering2 = toc;
  %  result.plottables.rotatedPositions_c2 = rotatedPositions_c2;
  %  result.plottables.IDX_c2 = IDX_c2;
  % result.plottables.sbase = sbase;
    %   result.plottables.IDX_lc = IDX_lc;
    %  result.plottables.rotatedPositions_lc = rotatedPositions_lc;
    %% STEP 4: PARAMETRIZATION
    tic
    disp('Tracing whiskers...');
    % rotatedPositions = rotatedPositions(IDX > 0,:);
    %  IDX = IDX(IDX>0);
    [lineGatherer_detector, result.MSE] = clusters2whiskers2(rotatedPositions,IDX, ...
        param_opt);                                                          % Now that the whiskers are clustered together, parametrize the whiskers in 4 parameters: position, angle, bending and length. <lineGatherer_detector> contains cells, every cell contains a vector of lenght 4 with these parameters in this order.
    result.lineGatherer_detector = lineGatherer_detector;
    for j = length(lineGatherer_detector):-1:1                               % It is possible that some clusters couldn't be parametrized. Those clusters are discarded.
        if isempty(lineGatherer_detector{j})
            IDX(IDX==j) = 0;
            IDX(IDX>j) = IDX(IDX>j)-1;
        end
    end
    lineGatherer_detector = lineGatherer_detector(~cellfun(@isempty, ...
        lineGatherer_detector));                                            % Also clean up this <lineGatherer_detector>, removing empty cells
    result.time.parametrization = toc;
    if ~dontTrack
        %% STEP 5: TRACKING & RECOGNITION
        tic;
        if start                                                                % If this is the first frame of the analysis, some things have to be set up.
            order = 1:length(lineGatherer_detector);                            % The order is just an ascending vector of number
            ranks = cell2mat(lineGatherer_detector');                           % Sort whiskes by position, so that it's easier to recognize them in the plot (highest whisker on the image first)
            [~, ranks] = sort(ranks(:,1),'ascend');
            lineGatherer_detector = lineGatherer_detector(ranks);
            lineGatherer_tracker = lineGatherer_detector;                       % All detected whiskers become tracks ...
            track_opt.couldNotFit = zeros(1,length(lineGatherer_tracker));      % ... so no missed whiskers yet.
            oldAverages = [mean(cellfun(@(x) x(1), ...
                lineGatherer_tracker(order > 0))) ...
                mean(cellfun(@(x) x(2), ...
                lineGatherer_tracker(order > 0))) 0 0];                         % The average of the whisker parameters is calculated, this is useful as training data.
            track_opt.prevXfAlpha = [mean(cellfun(@(x) ...
                x(1),lineGatherer_tracker)) ...
                mean((cellfun(@(x) x(2),lineGatherer_tracker)))];            % mean of xf and angle
            param_opt.minLength = param_opt.minLength2;
        else                                                                    % If this is NOT the first frame, the detected whiskers need to be related to the earlier ones.
            track_opt.prevXfAlpha = [mean(cellfun(@(x) x(1), ...
                lineGatherer_tracker)) mean((cellfun(@(x) x(2), ...
                lineGatherer_tracker)))];                                       % The average of the whisker parameters of the previous frame. This is useful for selection of training data, and also for estimating the position of whisker that could not be tracked.
            disp('Tracking whiskers...');
            if (i-startFrame <= train_opt.bootstrap) && ~loadPrevious           % During the initial `bootstrapping' period, we're only tracking, not recognizing.
                if useKalman                                                    % If configured, we use a K�lm�n-filter-like technique
                    [order, missedClusters, missedWhiskers] = ...
                        kalmanFitting( lineGatherer_tracker, ...
                        lineGatherer_detector, track_opt);
                else                                                            % Otherwise, we do Fit-whisker-point-to-data tracking
                    [order, missedClusters, missedWhiskers] = ...
                        fitWhiskers2clusters( rotatedPositions,IDX, ...
                        lineGatherer_tracker,lineGatherer_detector, ...
                        fwptd_opt);
                end
                
            else                                                                % After the `bootstrapping' period, we're working with SVM's
                [order, missedClusters, missedWhiskers] = ...
                    recognizer_Nexpert( lineGatherer_tracker, ...
                    lineGatherer_detector, recog_op, N_expert_op,track_opt);    % This function implements both the SVM recognition process and the N-expert.
                if ~isempty(missedClusters) && ~isempty(missedWhiskers)         % If not all the clusters and whiskers were matched, there's work to do for the P-expert
                    if useKalman                                                % Again, decide which algorithm to use as P-expert
                        [order_alt, ~, missedWhiskers_alt] = ...
                            kalmanFitting( lineGatherer_tracker, ...
                            lineGatherer_detector, track_opt);
                    else
                        [order_alt, ~, missedWhiskers_alt] = ...
                            fitWhiskers2clusters( rotatedPositions,IDX, ...
                            lineGatherer_tracker,lineGatherer_detector, ...
                            fwptd_opt);
                    end
                    for j = 1:length(missedWhiskers)                            % For every whisker that was not recognized by SVM but is supposed to be tracked
                        alternativeCluster = ...                                % Get assignment from P-expert
                            order_alt(missedWhiskers(j));
                        alternativeMatch = ...                                  % Did P-expert match missed whisker with unassigned cluster? Or didn't P-expert make a match either?
                            find(missedClusters==alternativeCluster);
                        if ~isempty(alternativeMatch)                           % If P-expert made a match...
                            order(missedWhiskers(j)) = ...                      % Fill it in in the 'order' array
                                order_alt(missedWhiskers(j));
                            if order(missedWhiskers(j)) > 0                     % If this was a 'real' assignment (so not just a zero), then output this.
                                disp(['Whisker ' ...
                                    num2str(missedWhiskers(j)) ...
                                    ' was found back by expert']);
                            end
                            missedClusters(alternativeMatch) = 0;               % This cluster is no longer missed
                            missedWhiskers(j) = 0;
                        end
                    end
                    missedWhiskers = missedWhiskers(missedWhiskers ~= 0);       % Remove all zeros.
                    missedClusters = missedClusters(missedClusters ~= 0);
                end
                
            end
            oldAverages = [mean(cellfun(@(x) ...
                x(1),lineGatherer_tracker(order > 0))) ...
                mean(cellfun(@(x) x(2),lineGatherer_tracker(order > 0)))];      % we still haven't changed lineGatherer_tracker, so here we're calculating the averages from the previous frame
            for j = 1:length(order)                                             % now we're going to put detected whiskers from frame n into the array 'lineGatherer_tracker'
                if order(j)
                    lineGatherer_tracker{j} = lineGatherer_detector{order(j)};
                end
            end
            newAverages = [mean(cellfun(@(x) x(1), ...
                lineGatherer_tracker(order > 0))) ...
                mean(cellfun(@(x) x(2),lineGatherer_tracker(order > 0)))];      % Now we're calculating the averages from the current frame
        end
        couldNotFit = zeros(1,length(lineGatherer_tracker));                    % Mark all the whiskers that are definitively missed in this frame.
        for j = 1:length(order)
            if ~order(j)
                lineGatherer_tracker{j}(1:2) = lineGatherer_tracker{j}(1:2) ... % And estimate their positon based on the movement of the rest of the whiskers.
                    - [oldAverages] + [newAverages];
                disp(['Could not detect whisker ' num2str(j) ...
                    ', estimating its position']);                              % Let the user know about this.
                %        pause
                couldNotFit(j) = 1;                                             % And also mark it in the results
            end
        end
        track_opt.couldNotFit = track_opt.couldNotFit.*couldNotFit+couldNotFit;
        result.time.whiskerFitting = toc;
        %% STEP 6: TRAINING
        tic
        currentMeanXf = mean(cellfun(@(x) x(1), lineGatherer_tracker));
        currentMeanA  = mean(cellfun(@(x) x(2), lineGatherer_tracker));
        if start
            track_opt.whiskerDB = cell(size(lineGatherer_tracker));
            track_opt.correctionXf = currentMeanXf;
            track_opt.correctionA = currentMeanA;
        end
        
        
        for d = 1:length(lineGatherer_tracker)
            if start
                track_opt.whiskerDB{d}.x1_p = [];
                track_opt.whiskerDB{d}.x2_p = [];
                track_opt.whiskerDB{d}.x3 = [];
                track_opt.whiskerDB{d}.L = [];
                track_opt.whiskerDB{d}.x1_a = [];
                track_opt.whiskerDB{d}.x2_a = [];
                track_opt.whiskerDB{d}.cnf = [];
                track_opt.whiskerDB{d}.prevW = cell(0);
                track_opt.whiskerDB{d}.nextW = cell(0);
            end
            track_opt.whiskerDB{d}.x1_p = [lineGatherer_tracker{d}(1) track_opt.whiskerDB{d}.x1_p];
            track_opt.whiskerDB{d}.x2_p = [(lineGatherer_tracker{d}(2)) track_opt.whiskerDB{d}.x2_p];
            track_opt.whiskerDB{d}.x3 = [lineGatherer_tracker{d}(3) track_opt.whiskerDB{d}.x3];
            track_opt.whiskerDB{d}.x1_a = [track_opt.prevXfAlpha(1) track_opt.whiskerDB{d}.x1_a];
            track_opt.whiskerDB{d}.x2_a = [track_opt.prevXfAlpha(2) track_opt.whiskerDB{d}.x2_a];
            track_opt.whiskerDB{d}.L = [lineGatherer_tracker{d}(4) track_opt.whiskerDB{d}.L];
            track_opt.whiskerDB{d}.cnf = [couldNotFit(d) track_opt.whiskerDB{d}.cnf];
            if order(d) == 1
                track_opt.whiskerDB{d}.prevW = [[lineGatherer_tracker{d}(1)- twptd_opt.maxPrevDist,-pi,-1,1] track_opt.whiskerDB{d}.prevW];
            elseif order(d) == 0
                track_opt.whiskerDB{d}.prevW = [[NaN NaN NaN NaN] track_opt.whiskerDB{d}.prevW];
            else
                track_opt.whiskerDB{d}.prevW = [lineGatherer_detector(order(d)-1) track_opt.whiskerDB{d}.prevW];
            end
            if order(d) == length(lineGatherer_detector)
                track_opt.whiskerDB{d}.nextW = [[lineGatherer_tracker{d}(1)+ twptd_opt.maxPrevDist,pi,1,1] track_opt.whiskerDB{d}.nextW];
            elseif order(d) == 0
                track_opt.whiskerDB{d}.nextW = [[NaN NaN NaN NaN] track_opt.whiskerDB{d}.nextW];
            else
                track_opt.whiskerDB{d}.nextW = [lineGatherer_detector(order(d)+1) track_opt.whiskerDB{d}.nextW];
            end
            if length(track_opt.whiskerDB{d}.x1_p) > train_opt.window
                track_opt.whiskerDB{d}.x1_p = track_opt.whiskerDB{d}.x1_p(1:train_opt.window);
                track_opt.whiskerDB{d}.x2_p = track_opt.whiskerDB{d}.x2_p(1:train_opt.window);
                track_opt.whiskerDB{d}.x1_a = track_opt.whiskerDB{d}.x1_a(1:train_opt.window);
                track_opt.whiskerDB{d}.x2_a = track_opt.whiskerDB{d}.x2_a(1:train_opt.window);
                track_opt.whiskerDB{d}.L =    track_opt.whiskerDB{d}.L(1:train_opt.window);
                track_opt.whiskerDB{d}.cnf = track_opt.whiskerDB{d}.cnf(1:train_opt.window);
                track_opt.whiskerDB{d}.x3 = track_opt.whiskerDB{d}.x3(1:train_opt.window);
                track_opt.whiskerDB{d}.prevW = track_opt.whiskerDB{d}.prevW(1:train_opt.window);
                track_opt.whiskerDB{d}.nextW = track_opt.whiskerDB{d}.nextW(1:train_opt.window);
            end
        end
        track_opt.previousMeanXf = currentMeanXf;
        track_opt.previousMeanA = currentMeanA;
        counter = 1;
        ks = [];
        js = [];
        for k = 2:length(lineGatherer_tracker)
            for j = 1:(k-1)
                ks = [ks k];
                js = [js j];
            end
        end
        if ((i-startFrame > train_opt.bootstrap) && ~mod(i,min(train_opt.retrainFreqInit,train_opt.retrainAtLeast))) || (i-startFrame == train_opt.bootstrap)
            currentTrains = cell(1,length(ks));
            for k = 1:length(ks)
                X_1 = prepareTrainingData(track_opt.whiskerDB{ks(k)},track_opt.prevXfAlpha(2), train_opt.maxSizeTrainingData,param_opt.minAngleToSnout);
                X_2 = prepareTrainingData(track_opt.whiskerDB{js(k)},track_opt.prevXfAlpha(2), train_opt.maxSizeTrainingData,param_opt.minAngleToSnout);
                currentTrains{k} = [X_1 ones(size(X_1,1),1)*ks(k); X_2 ones(size(X_2,1),1)*js(k)];
            end
            recog_op.svm_matrix = cell(length(track_opt.whiskerDB));
            svm_matrix = cell(1,length(ks));
            tic
            parfor k = 1:length(ks)
                currentTrain = currentTrains{k};
                yesOrNo = rand(size(currentTrain,1),1) < train_opt.chanceTrain;
                currentTrain = currentTrain(yesOrNo,:)
                Y = currentTrain(:,end);
                X = currentTrain(:,1:end-1);
                %      M = fitcensemble(X,Y)
                %       M = fitcknn(X,Y,'NumNeighbors',60,'Standardize',1)
                M = fitcsvm(X,Y,'Standardize',true,'KernelScale','auto','OutlierFraction',0.20);%,'KernelFunction','polynomial','PolynomialOrder',2);%,'OptimizeHyperparameters','auto','Verbose',0);%'HyperparameterOptimizationOptions',hyperparOp);
                svm_matrix{k} = M;
            end
            result.time.trainTime = toc;
            if result.time.trainTime > maxRetrainTime
                disp(['Retraining took more than ' num2str(maxRetrainTime) ' seconds. Saving and quitting...'])
                save([outputFolder '\' num2str(i) '_diverged']);
                break;
            end
            disp(['Retrained SVMs in ' num2str(result.time.trainTime) ...
                ' seconds.']);
            for k = 1:length(ks)
                recog_op.svm_matrix{ks(k),js(k)} = svm_matrix{k};
            end
        end
        result.time.svmTrain = toc;
        if start
            couldNotFit = zeros(size(lineGatherer_tracker));
        end
        result.lineGatherer_tracker = lineGatherer_tracker;
        if ~start
            result.missedWhiskers = missedWhiskers;
        else
            result.missedWhiskers = cell(0);
        end
        result.order = order;
        result.couldNotFit = couldNotFit;
        result.lineGatherer_detector = lineGatherer_detector;
    else
        result.lineGatherer_detector = lineGatherer_detector;
        result.couldNotFit = {};
        result.order = {};
    end
    allResults{i} = result;                                         % Gather the results
    if start
        inputs = [];
        targets = [];
    end
    if ~mod(i,train_opt.increaseEvery)
        train_opt.retrainFreqInit = train_opt.retrainFreqInit + train_opt.increaseWith;
    end
    %    if batchMode || loadPrevious
    if ~mod(i,saveEvery)
%        save([outputFolder '\' num2str(i)]);
    end
    if (noPlotting && pauseOn ~= i) && (plotEvery == 0 || mod(i,plotEvery) ~= 0)%&& ~sum(missedWhiskers)
        start = 0;
        continue;
    end
    %  end
    %% PLOTTING RESULTS
    plottablePositions = [sbase^-1*rotatedPositions_c2']';                     % The code from here on just plots the preprocessed image,
    plottablePositions(:,2) = -plottablePositions(:,2);                     % the detected clusters and the position of the lines, so that the user can
    figure(1);                                                                % check if it is accurate.
    clf
    subplot(2,3,3);
    title('Clusters after cluster stitching')
    str = exist('axisSetup');
    if ~exist('axisSetup')
        PlotClusterinResult(plottablePositions, IDX_c2);
        axisSetup = gca;
        axisSetup = [axisSetup.XLim axisSetup.YLim];
        loadPrevious = 0;
    else
        scatter([0 0],[0 0]);
        hold on
        axis(axisSetup);
        PlotClusterinResult(plottablePositions, IDX_c2);
    end
    plottablePositions_lc = [sbase^-1*rotatedPositions_lc']';                     % The code from here on just plots the preprocessed image,
    plottablePositions_lc(:,2) = -plottablePositions_lc(:,2);                     % the detected clusters and the position of the lines, so that the user can
    subplot(2,3,2);
    title('Clusters after local clustering')
    axis(axisSetup);
    PlotClusterinResult(plottablePositions_lc, IDX_lc);
    subplot(2,3,1);
    title('Upscaled image, preprocessed')
    imagesc(upscaledImage);
    hold on;
    line([tip(1) middle(1)],[tip(2) middle(2)],'color','r','LineWidth',2);
    hold off;
    subplot(2,3,4);
    hold on
    for d = 1:length(lineGatherer_detector)
        myLine = plotParLine(lineGatherer_detector{d}(1:4));
        myLine = [sbase^-1*myLine']';
        myLine(:,2) = -myLine(:,2);
        axis(axisSetup);
        hp = plot(myLine(:,1),myLine(:,2),'DisplayName',num2str(d));
    end
    legend('show')
    title('Detected lines');
    hold off
    if ~dontTrack
        subplot(2,3,5);
        hold on
        scatterColors = cell(1,length(lineGatherer_tracker));
        for d = 1:length(lineGatherer_tracker)
            if isempty(lineGatherer_tracker{d})
                continue;
            end
            myLine = plotParLine(lineGatherer_tracker{d}(1:4));
            myLine = [sbase^-1*myLine']';
            myLine(:,2) = -myLine(:,2);
            if couldNotFit(d) == 1
                lineSpec = '--';
            elseif couldNotFit(d) == 0
                lineSpec = '-';
            else
                lineSpec = ':';
            end
            hp = plot(myLine(:,1),myLine(:,2),lineSpec,'DisplayName',num2str(d));
            scatterColors{d} = get(hp,'Color');
            axis(axisSetup);
        end
        title('Tracked whiskers');
        legend('show')
        subplot(2,3,6);
        title('x-position vs. cot(alpha)')
        hold on
        for d = 1:length(lineGatherer_tracker)
            x = track_opt.whiskerDB{d}.x1_p;%-knn.whiskerDB{d}.x1_a;
            y = cot(track_opt.whiskerDB{d}.x2_p);%-knn.whiskerDB{d}.x2_a;
            scatter(x,y,'.','MarkerFaceColor',scatterColors{d},'DisplayName',num2str(d));
            hold on;
        end
        legend('show')
    end
    drawnow
    if start || i == pauseOn
        disp('press any key to continue');
        start = 0;
        pause;
    end
end
save([outputFolder '\' num2str(i)]);
