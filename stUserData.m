function [out1, out2] = stUserData(key, value)

% [values, prefs] = stUserData()
%
%   This form returns complete User Data information,
%   including all values (in "values") and the preferences
%   structure (in "prefs") that can be used to display the
%   Preferences GUI.
%
% stUserData(key, value)
%
%   This form sets the value of a specified entry in the
%   User Data.
%
% value = stUserData(key)
%
%   This form retrieves the value of a specified entry in
%   the User Data. If "key" is suffixed with "!",
%   post-processing is performed (post-processing is
%   key-specific).
%
%
%
% See also: stRevision, stProject, stJob, stPlugin, stMergePars, stUserData, stLoadMovieFrames.



%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====




% Author: Ben Mitch
% Created: 06/02/2011
%
% Because the world is pretty and sunny and
% filled with little rabbits and other beautiful things,
% shame on you if you make it less beautiful. But do as
% you will with this code, the little rabbits will not
% mind.

% 20/11/14 (mitch)
% added option 'videoreader' to pref 'videoReader'









%% PERSISTENT DATA

persistent prefs userdata filename



%% GET FILENAME

if isempty(filename)
	
	% get userdata file location
	w = which('stLocal');
	
	% if available, get filename from that
	path = [];
	if ~isempty(w)
		
		% supplied by local configuration
		path = stLocal('getUserDataPath');
		
	end
	
	% if not returned
	if isempty(path)
		
		% use default location
		path = getAppDataPath();
		
	end

	% make directory if not already
	if ~exist(path, 'dir')
		success = mkdir(path);
		if ~success
			error('BWTT:stUserData:FailedMakeUserDataPath', ...
				['failed to create userdata folder "' path '"']);
		end
	end

	% get filename
	oldfilename = [path '/stUserData.mat'];
	filename = [path '/userdata.mat'];
	
	% attempt to move old file
	if exist(oldfilename, 'file') && ~exist(filename, 'file')
		movefile(oldfilename, filename);
	end
		
end



%% GET USERDATA

if isempty(userdata)
	
	% handle legacy storage location on windows
	loadFilename = filename;
	if ispc && ~exist(loadFilename)
		loadFilename = [getenv('USERPROFILE') '/standardTracker_userData.mat'];
	end
	
	% load userdata
	if exist(loadFilename, 'file')

		% only use it if it is version 2 or above
		loaded = load(loadFilename);
		if isfield(loaded.userdata, 'version') && loaded.userdata.version >= 2
			userdata = loaded.userdata;
		end
		
	end
	
	% default userdata
	if isempty(userdata)
		
		userdata = [];
		userdata.version = 4;
		userdata.values = struct();
		
	end
	
	% upgrade version
	if userdata.version == 2
		
		userdata.values = userdata.prefs;
		fn = fieldnames(userdata.meta);
		for n = 1:length(fn)
			userdata.values.(fn{n}) = userdata.meta.(fn{n});
		end
		userdata = rmfield(userdata, 'prefs');
		userdata = rmfield(userdata, 'meta');
		userdata = rmfield(userdata, 'servers');
		
		% bump version
		userdata.version = 3;
		
	end
	
	% upgrade version
	if userdata.version == 3
		
		% this value is replaced by "videoServers"
		if isfield(userdata.values, 'networkShares')
			shares = userdata.values.networkShares;
			userdata.values = rmfield(userdata.values, 'networkShares');
			userdata.values.videoServers = splitNetworkShares(shares);
		end
		
		% bump version
		userdata.version = 4;
		
	end
	
end



%% GET PREFS

if isempty(prefs)
	
	% empty struct
	prefs = struct();
	
	% video reader
	prefs.videoReader = struct( ...
		'label', 'Video reader', ...
		'type', 'option', ...
		'options', {{'automatic' 'mmreader' 'mmread' 'aviread' 'videoreader'}}, ...
		'default', 'automatic' ...
		);
	
	% fixed width font
	[list, def] = getFonts();
	prefs.listFontName = struct( ...
		'label', 'Listbox font name', ...
		'type', 'option', ...
		'options', {list}, ...
		'default', def ...
		);
	
	% font size
	prefs.listFontSize = struct( ...
		'label', 'Listbox font size', ...
		'type', 'option', ...
		'options', {{8 9 10 11 12}}, ...
		'default', 10 ...
		);
	
	% big screen
	prefs.bigScreen = struct( ...
		'help', 'When this flag is set, some display options are changed: key frame is displayed in the job list.', ...
		'label', 'Big screen mode (requires restart)', ...
		'type', 'flag', ...
		'default', false ...
		);
	
	% temporary directory
	prefs.temporaryDirectory = struct( ...
		'label', 'Temporary directory', ...
		'type', 'dir', ...
		'default', '' ...
		);
	
% 	% autosave history
% 	prefs.autoSaveHistory = struct( ...
% 		'label', 'Historical saves to keep (in Temporary Directory)', ...
% 		'type', 'option', ...
% 		'options', {{'None' 3 5 10 25}}, ...
% 		'default', 10 ...
% 		);
% 	
% 	% autosave interval
% 	prefs.autoSaveInterval = struct( ...
% 		'label', 'Minutes before autosave', ...
% 		'type', 'option', ...
% 		'options', {{1 2 3 5 10 15 30}}, ...
% 		'default', 5 ...
% 		);
	
	% preview policy
	prefs.previewPolicy = struct( ...
		'label', 'Build previews...', ...
		'type', 'option', ...
		'options', {{'On disk' 'In memory' 'Never'}}, ...
		'colbreak', 1, ...
		'default', 'On disk' ...
		);
	
	% video servers
	prefs.videoServers = struct( ...
		'label', 'Video Servers', ...
		'type', 'servers', ... % this means - must be {UPPER_CASE tidyFoldername} form
		'default', {{}} ...
		);
	
% 	% immediate pars update
% 	prefs.immediateParametersUpdate = struct( ...
% 		'label', 'Update parameters on load of parameters GUI', ...
% 		'type', 'flag', ...
% 		'default', false ...
% 		);
	
	% window position management
	prefs.rememberWindowPosition = struct( ...
		'label', 'Window position when opening main GUI', ...
		'type', 'option', ...
		'options', {{'Not managed' 'Position when last closed' 'Position right now'}}, ...
		'default', 'Not managed' ...
		);
	
	% window position management
	prefs.showCARequirement = struct( ...
		'label', 'Show requirements for co-authorship', ...
		'type', 'flag', ...
		'default', true ...
		);
	
	% these remaining values are ignored by stPreferencesGUI,
	% but allow these values to be set/get without error
	
	% recent projects list
	prefs.recentProjects = struct( ...
		'postproc', @removeEmptyFromList, ...
		'default', {{}} ...
		);
	
	% mru project dir
	prefs.lastUsedProjectDirectory = struct( ...
		'default', '' ...
		);
	
	% stored window position
	prefs.storedWindowPosition = struct( ...
		'default', [] ...
		);
	
% 	% EULA flag
% 	prefs.userHasAcceptedEULA = struct( ...
% 		'default', false ...
% 		);

	% version information
	prefs.lastLoadedRevision = struct( ...
		'default', 0 ...
		);

	% normalise switch in preview window
	prefs.normaliseFrame = struct( ...
		'default', false ...
		);
	
	% development mode
	prefs.devMode = struct( ...
		'default', false ...
		);
	
	% assert - there should be no fields in userdata.values that
	% do not appear in prefs (if there are, they are hangovers
	% from old no longer existing preference keys)
	fn = fieldnames(userdata.values);
	for n = 1:length(fn)
		if ~isfield(prefs, fn{n})
			warning(['userdata field "' fn{n} '" not found in prefs - it has been discarded']);
			userdata.values = rmfield(userdata.values, fn{n});
		end
	end
	
end



%% OPERATION

switch sprintf('%i%i', [nargout nargin])
	
	case '11'
		
		% handle postproc
		postproc = any(key == '!');
		key = key(key ~= '!');
		
		% validate arguments
		if ~isfield(prefs, key)
			error('BWTT:stUserData:KeyNotFound', ...
				['the field "' key '" was not found']);
		end
		
		% get specified value
		if isfield(userdata.values, key)
			out1 = userdata.values.(key);
		else
			out1 = prefs.(key).default;
		end
		
		% do post-processing
		if postproc && isfield(prefs.(key), 'postproc')
			out1 = prefs.(key).postproc(out1);
		end
		
		
		
	case '02'
		
		% validate
		if ~isfield(prefs, key)
			error('BWTT:stUserData:KeyNotFound', ...
				['the field "' key '" was not found']);
		end
		
		% validate
		if isfield(prefs.(key), 'type')
			switch prefs.(key).type

				case 'option'
					if ~any(isin(prefs.(key).options, value))
						error('BWTT:stUserData:ValueNotValid', ...
						['the value for field "' key '" was invalid']);
					end

				case 'flag'
					if ~isscalar(value)
						error('BWTT:stUserData:ValueNotValid', ...
						['the value for field "' key '" was invalid']);
					end
					value = value ~= 0;

				case 'dir'
					if ~exist(value, 'dir')
						error('BWTT:stUserData:ValueNotValid', ...
						['the value for field "' key '" was invalid']);
					end
					value = stTidyFilename(value);

			end
		end
		
		% set specified value
		userdata.values.(key) = value;
		
		% save userdata
		save(filename, 'userdata');
		
		
		
	case {'10' '20'}
		
		% get prefs data for display of GUI
		out1 = userdata.values;
		out2 = prefs;
		
		
		
		
	otherwise
		error('BWTT:stUserData:OperationNotRecognised', ...
			['the calling convention was not recognised']);
		
end





%% HELPERS (USED ONLY FOR UPGRADE FROM v0.3 TO v0.4)

function c = splitNetworkShares(s)

if ~isempty(s) && s(end) ~= pathsep
	s = [s pathsep];
end
ss = stStringExplode(pathsep, s);
ss = ss(1:end-1);
c = {};
for n = 1:length(ss)
	s = ss{n};
	eq = find(s == '=');
	if isempty(eq)
		warning(['Ignoring invalid entry in "networkShares", "' s '"']);
	end
	c{end+1, 1} = {s(1:eq-1) s(eq+1:end)};
end




%% POST-PROCESSING

function list = removeEmptyFromList(list) % no user doc %

% Goren once got an empty entry in his recent projects list
% - not sure if this can still happen, but for now we clean
% it up, here.
%
% UPDATE: 06/05/11 think this is fixed now, it was caused by
% the line recent = recent([1:r-1 r+1:end]) which both
% trimmed but also flipped to a horizontal array the recent
% files list iff this left it empty. i've fixed the line, so
% probably won't happen any more. however, i'll leave this
% here in case, no harm in it.

keep = [];
for l = 1:length(list)
	if ~isempty(list{l})
		keep = [keep l];
	else
		warning('removed empty entry from recent projects list');
	end
end
list = list(keep);





%% APPDATA PATH

function path = getAppDataPath()

if ispc
	path = getenv('APPDATA');
else
	path = getenv('HOME');
end

if isempty(path)
	error('BWTT:stUserData:FailedGetAppDataPath', ...
		'failed to get user''s application data folder');
end

if ispc
	oldpath = [path '/StandardTracker'];
	path = [path '/BWTT'];
else
	oldpath = [path '/.StandardTracker'];
	path = [path '/.BWTT'];
end

if exist(oldpath, 'dir') && ~exist(path, 'dir')
	% attempt to move directory - if this fails, user may have
	% to intervene
	movefile(oldpath, path);
end



%% FONTS

function [list, def] = getFonts()

list = {'Andale Mono' 'Courier New' 'Monaco' 'Lucida Console' 'Consolas' 'Courier' 'DejaVu Sans Mono' ...
	'Droid Sans Mono' 'Everson Mono' 'Fixedsys' 'Hyperfont' 'Letter Gothic' 'Monofur' 'Prestige' ...
	'Inconsolata' 'Pro Font' 'Prestige Elite' 'Tex Gyre Cursor' 'UM Typewriter'};
avail = listfonts;
list = list(ismemberi(list, avail));

def = list{1};
list = sort(list);


function i = ismemberi(test, set)

i = logical([]);

for n = 1:length(test)
	i(n) = 0;
	for s = 1:length(set)
		if strcmpi(set{s}, test{n})
			i(n) = 1;
			break
		end
	end
end




%% ISIN


function index = isin(list, value)

if ischar(value)
	
	index = ismember(list, value);
	
else
	
	index = [];
	for i = 1:length(list)
		if value == list{i}
			index(end+1) = i;
		end
	end

end






