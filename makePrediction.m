function [predictions_Xf, predictions_a, predictions_B, predictions_L] = makePrediction(opts,lineGatherer_tracker)
%MAKEPREDICTION Summary of this function goes here
%   Detailed explanation goes here
predictions = cell(size(lineGatherer_tracker));

predictions_Xf = zeros(size(lineGatherer_tracker));
predictions_a = zeros(size(lineGatherer_tracker));
predictions_L = zeros(size(lineGatherer_tracker));
predictions_B = zeros(size(lineGatherer_tracker));
for i = 1:length(lineGatherer_tracker)
    necc_xf = opts.whiskerDB{i}.x1_p;
    necc_a = opts.whiskerDB{i}.x2_p;
    necc_L = opts.whiskerDB{i}.L;
    necc_b = opts.whiskerDB{i}.x3;
    if length(necc_xf) > opts.kalmanDistance
        necc_xf = necc_xf(1:opts.kalmanDistance);
        necc_a = necc_a(1:opts.kalmanDistance);
        necc_L = necc_L(1:opts.kalmanDistance);
        necc_b = necc_b(1:opts.kalmanDistance);
    end
    if (length(necc_xf) < opts.kalmanDistance) || (opts.kalmanDistance <= 1)
        predictions_Xf(i) = mean(necc_xf);
        predictions_a(i) = mean(necc_a);
        predictions_L(i) = mean(necc_L);
        predictions_B(i) = mean(necc_b);
    else
        p = polyfit(1:length(necc_xf),necc_xf,2);
        predictions_Xf(i) = p(3);
        p = polyfit(1:length(necc_a),necc_a,2);
        predictions_a(i) = p(3);
        predictions_L(i) = mean(necc_L);
        predictions_B(i) = mean(necc_b);
    end
end
end
