function [ distancesq ] = distanceToLine( xf, alpha, b, dots)
dots = dots - complex(xf);
x_sh = cos(alpha)*real(dots)+sin(alpha)*imag(dots);
y_sh = -sin(alpha)*real(dots)+cos(alpha)*imag(dots);
distancesq = complex((b*x_sh.^2-y_sh).^2) + x_sh*1i;
end